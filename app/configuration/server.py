"""Server configuration."""
from typing import TypeVar

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.pkg.models.base import BaseAPIException
from app.internal.pkg.middlewares.handle_http_exceptions import handle_api_exceptions, handle_internal_exception
from app.internal.routes import __routes__

__all__ = ["Server"]

FastAPIInstance = TypeVar("FastAPIInstance", bound="FastAPI")


class Server:

    def __init__(self, app: FastAPI):
        self.__app = app
        self._register_routes(app)
        self._register_http_exceptions(app)
        self._register_middlewares(app)

    def get_app(self) -> FastAPIInstance:
        return self.__app

    @staticmethod
    def _register_routes(app: FastAPIInstance) -> None:
        __routes__.allocate_routes(app)

    @staticmethod
    def _register_http_exceptions(app: FastAPIInstance):
        """Register http exceptions.

        FastAPIInstance handle BaseApiExceptions raises inside functions.

        Args:
            app: ``FastAPI`` application instance

        Returns: None
        """

        app.add_exception_handler(BaseAPIException, handle_api_exceptions)
        app.add_exception_handler(Exception, handle_internal_exception)

    @staticmethod
    def __register_cors_origins(app: FastAPIInstance) -> None:
        """Register cors origins."""

        app.add_middleware(
            CORSMiddleware,
            allow_origins=["*"],
            allow_methods=["*"],
            allow_headers=["*"],
            allow_credentials=True
        )

    def _register_middlewares(self, app) -> None:
        """Apply routes middlewares."""

        self.__register_cors_origins(app)
