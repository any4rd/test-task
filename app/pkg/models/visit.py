from pydantic import Field
from pydantic.types import PositiveInt

from .base import BaseModel

__all__ = [
    "VisitFields",
    "Visit"
]


class VisitFields:
    id = Field(description="id пользователя", example=2)
    visit = Field(description="ФИО", example="Иванов Иван Иванович")


class BaseVisit(BaseModel):
    """Base model for user."""


class Visit(BaseVisit):
    id: PositiveInt = VisitFields.id
    visit: str = VisitFields.visit
