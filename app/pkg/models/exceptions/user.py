from fastapi import status

from app.pkg.models.base import BaseAPIException


class UserAlreadyExist(BaseAPIException):
    status_code = status.HTTP_409_CONFLICT
    message = "User already exist."


class UserNotFound(BaseAPIException):
    status_code = status.HTTP_404_NOT_FOUND
    message = "User was not found."

