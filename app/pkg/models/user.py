from datetime import date

from pydantic import EmailStr, Field
from pydantic.types import PositiveInt, Optional

from .base import BaseModel

__all__ = [
    "User",
    "UserFields",
    "BaseModel",
    "CreateUserCommand",
    "ReadAllUsersQuery",
    "ReadUserByIdQuery",
    "UpdateUserCommand",
    "DeleteUserCommand",
    "SearchWords"
]


class UserFields:
    id = Field(description="id пользователя", example=2)
    username = Field(description="ФИО", example="Иванов Иван Иванович")
    birthday = Field(description="Дата рождения", example="1987-10-05")
    gender = Field(description="Пол (False/мужской, True/женский)", example=False)
    created_at = Field(description="Дата создания", example="2023-04-08")
    updated_at = Field(description="Дата обновления", example="2023-04-09")
    email = Field(description="E-mail адрес", example="example@example.ru")
    search_words = Field(description="Строка поиска", example="Иван")


class BaseUser(BaseModel):
    """Base model for user."""


class User(BaseUser):
    id: PositiveInt = UserFields.id
    username: str = UserFields.username
    birthday: date = UserFields.birthday
    gender: bool = UserFields.gender
    created_at: Optional[date] = UserFields.created_at
    updated_at: Optional[date] = UserFields.updated_at
    email: EmailStr = UserFields.email


# Commands.
class CreateUserCommand(BaseUser):
    username: str = UserFields.username
    birthday: date = UserFields.birthday
    gender: bool = UserFields.gender
    email: EmailStr = UserFields.email


class UpdateUserCommand(BaseUser):
    id: PositiveInt = UserFields.id
    username: Optional[str] = UserFields.username
    gender: Optional[bool] = UserFields.gender
    updated_at: date
    email: Optional[EmailStr] = UserFields.email


class DeleteUserCommand(BaseUser):
    id: PositiveInt = UserFields.id


# Query
class ReadAllUsersQuery(BaseUser):
    ...


class ReadUserByIdQuery(BaseUser):
    id: PositiveInt = UserFields.id


class SearchWords(BaseUser):
    search_words: str = UserFields.search_words
