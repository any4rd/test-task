"""User service."""
from typing import List, Optional

from app.internal.repository.postgresql import user
from app.internal.repository.repository import BaseRepository
from app.pkg import models
from app.pkg.models.exceptions.repository import UniqueViolation, EmptyResult
from app.pkg.models.exceptions.user import UserAlreadyExist, UserNotFound

__all__ = ["User"]


class User:
    repository: user.User

    def __init__(self, repository: BaseRepository):
        self.repository = repository

    async def create_user(self, cmd: models.CreateUserCommand) -> models.User:
        try:
            return await self.repository.create(cmd=cmd)
        except UniqueViolation:
            raise UserAlreadyExist

    async def read_all_users(
        self
    ) -> List[models.User]:
        """Read all users from repository."""
        try:
            return await self.repository.read_all()
        except EmptyResult:
            raise UserNotFound

    async def read_specific_user_by_id(
        self,
        query: models.ReadUserByIdQuery,
    ) -> models.User:
        """Read specific user from repository by user id."""
        try:
            return await self.repository.read(query=query)
        except EmptyResult:
            raise UserNotFound

    async def delete_specific_user(
        self,
        cmd: models.DeleteUserCommand,
    ) -> models.User:
        """Delete specific user by user id."""
        try:
            return await self.repository.delete(cmd=cmd)
        except EmptyResult:
            raise UserNotFound

    async def update_user_by_id(
        self,
        cmd: models.UpdateUserCommand
    ) -> models.User:
        """Read specific user from repository by user id."""
        try:
            return await self.repository.update(cmd=cmd)
        except EmptyResult:
            raise UserNotFound

    async def search(
        self,
        cmd: models.SearchWords
    ) -> List[models.User]:
        """Search by name and email."""
        try:
            return await self.repository.search(cmd=cmd)
        except EmptyResult:
            raise UserNotFound
