from typing import List

from fastapi import Response

from app.internal.repository.postgresql import user, visit
from app.internal.repository.repository import BaseRepository
from app.pkg import models
from app.pkg.models.exceptions.repository import UniqueViolation
from app.pkg.models.exceptions.user import UserAlreadyExist

__all__ = ["Visit"]


class Visit:
    repository: visit.Visit

    def __init__(self, repository: BaseRepository):
        self.repository = repository

    async def create_visit(self, cmd: str) -> models.Visit:
        try:
            return await self.repository.create(cmd=cmd)
        except UniqueViolation:
            raise UserAlreadyExist

    async def read_all_visits(
            self
    ) -> List[models.Visit]:
        """Read all users from repository."""
        return await self.repository.read_all()
