from typing import List

from dependency_injector.wiring import Provide, inject
from fastapi import APIRouter, Depends, status
from pydantic import PositiveInt

from app.internal import services
from app.pkg import models

router = APIRouter(prefix="/users", tags=["Users"])


@router.post(
    "/",
    response_model=models.User,
    status_code=status.HTTP_201_CREATED,
    summary="Create user.",
)
@inject
async def create_user(
        cmd: models.CreateUserCommand,
        user_service: services.User = Depends(Provide[services.Services.user]),
):
    return await user_service.create_user(cmd=cmd)


@router.get(
    "/",
    response_model=List[models.User],
    status_code=status.HTTP_200_OK,
    summary="Get all users.",
)
@inject
async def read_all_users(
        user_service: services.User = Depends(Provide[services.Services.user]),
):
    return await user_service.read_all_users()


@router.get(
    "/{user_id:int}",
    response_model=models.User,
    status_code=status.HTTP_200_OK,
    summary="Read user by id.",
)
@inject
async def read_user(
        user_id: PositiveInt = models.ReadUserByIdQuery,
        user_service: services.User = Depends(Provide[services.Services.user]),
):
    return await user_service.read_specific_user_by_id(
        query=models.ReadUserByIdQuery(id=user_id),
    )


@router.delete(
    "/{user_id:int}",
    response_model=models.User,
    status_code=status.HTTP_200_OK,
    summary="Delete specific user.",
)
@inject
async def delete_user(
        user_id: PositiveInt = models.DeleteUserCommand,
        user_service: services.User = Depends(Provide[services.Services.user]),
):
    return await user_service.delete_specific_user(
        cmd=models.DeleteUserCommand(id=user_id),
    )


@router.put(
    "/",
    response_model=models.User,
    status_code=status.HTTP_200_OK,
    summary="Update user by id.",
)
@inject
async def read_user(
        cmd: models.UpdateUserCommand,
        user_service: services.User = Depends(Provide[services.Services.user]),
):
    return await user_service.update_user_by_id(cmd=cmd)


@router.get(
    "/search_words/{search_words:str}",
    response_model=List[models.User],
    status_code=status.HTTP_200_OK,
    summary="Search users.",
)
@inject
async def search(
        search_words: str,
        user_service: services.User = Depends(Provide[services.Services.user]),
):
    return await user_service.search(cmd=models.SearchWords(search_words=search_words))

