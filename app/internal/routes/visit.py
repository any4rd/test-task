from datetime import datetime
from typing import List
from uuid import uuid4

from dependency_injector.wiring import inject, Provide
from fastapi import APIRouter, status, Response, Depends

from app.internal import services
from app.internal.pkg.middlewares.session import SessionData, backend, cookie
from app.pkg import models

router = APIRouter(tags=["Visit"])


@router.get(
    '/',
    response_model=models.Visit,
    status_code=status.HTTP_200_OK,
    summary="create visit user.",
)
@inject
async def create_session(
        response: Response,
        visit_service: services.Visit = Depends(Provide[services.Services.visit])
):
    name = str(datetime.now())
    session = uuid4()
    data = SessionData(username=name)

    await backend.create(session, data)
    cookie.attach_to_response(response, session)
    return await visit_service.create_visit(cmd=name)


@router.get(
    "/visit",
    response_model=List[models.Visit],
    status_code=status.HTTP_200_OK,
    summary="Get all visit.",
)
@inject
async def read_all_visit(
        user_service: services.Visit = Depends(Provide[services.Services.visit]),
):
    return await user_service.read_all_visits()
