from datetime import datetime
from uuid import uuid4

from fastapi import Security, Response
from fastapi.security import APIKeyHeader

from app.internal.pkg.middlewares.session import SessionData, backend, cookie
from app.pkg.models.exceptions.x_auth_token import InvalidCredentials
from app.pkg.settings import settings

__all__ = ["get_x_token_key"]

x_api_key_header = APIKeyHeader(name="X-API-TOKEN")


async def get_x_token_key(
    api_key_header: str = Security(x_api_key_header),
):
    value = settings.X_API_TOKEN.get_secret_value()
    if api_key_header != value:
        raise InvalidCredentials
