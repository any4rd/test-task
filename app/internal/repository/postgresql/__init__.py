from dependency_injector import containers, providers

from .user import User
from .visit import Visit

__all__ = ["Repository"]


class Repository(containers.DeclarativeContainer):
    user = providers.Factory(User)
    visit = providers.Factory(Visit)
