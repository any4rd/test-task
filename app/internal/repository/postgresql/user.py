from typing import List

from app.internal.repository.handlers.postgresql.collect_response import (
    collect_response,
)
from app.internal.repository.postgresql.connection import get_connection
from app.internal.repository.repository import Repository
from app.pkg import models

__all__ = ["User"]


class User(Repository):
    @collect_response
    async def create(self, cmd: models.CreateUserCommand) -> models.User:
        q = """
            insert into users(
                username, birthday, gender, email
            )
                values (
                    %(username)s, %(birthday)s, %(gender)s, %(email)s
                )
            returning id, username, birthday, gender, created_at, email;
        """
        async with get_connection() as cur:
            await cur.execute(q, cmd.to_dict(show_secrets=True))
            return await cur.fetchone()

    @collect_response
    async def read(self, query: models.ReadUserByIdQuery) -> models.User:
        q = """
            select 
                id, username, birthday, gender, created_at, email
            from users
            where users.id = %(id)s;
        """
        async with get_connection() as cur:
            await cur.execute(q, query.to_dict(show_secrets=True))
            return await cur.fetchone()

    @collect_response
    async def read_all(self) -> List[models.User]:
        q = """
            select 
                 id, username, birthday, gender, created_at, updated_at, email
            from users;
        """
        async with get_connection() as cur:
            await cur.execute(q)
            return await cur.fetchall()

    @collect_response
    async def update(self, cmd: models.UpdateUserCommand) -> models.User:
        q = """
            update users 
                set 
                    username = coalesce(%(username)s, username),
                    gender = coalesce(%(gender)s, gender), 
                    updated_at = coalesce(%(updated_at)s, updated_at), 
                    email = coalesce(%(email)s, email)
                where id = %(id)s
            returning 
                id, username, birthday, gender, updated_at, email;
        """
        async with get_connection() as cur:
            await cur.execute(q, cmd.to_dict())
            return await cur.fetchone()

    @collect_response
    async def delete(self, cmd: models.DeleteUserCommand) -> models.User:
        q = """
            delete from users where id = %(id)s
            returning id, username, birthday, gender, updated_at, email;
        """
        async with get_connection() as cur:
            await cur.execute(q, cmd.to_dict(show_secrets=True))
            return await cur.fetchone()

    @collect_response
    async def search(self, cmd: models.SearchWords) -> List[models.User]:
        search_words = f"%{cmd.search_words}%"
        q = """
            SELECT users.id, users.username, users.birthday, users.gender, 
                   users.created_at, users.updated_at, users.email
            FROM public.users
            WHERE users.username ILIKE %(search_words)s 
                  OR users.email ILIKE %(search_words)s;
        """
        async with get_connection() as cur:
            await cur.execute(q, {'search_words': search_words})
            return await cur.fetchall()
