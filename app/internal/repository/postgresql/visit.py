from typing import List

from app.internal.repository.handlers.postgresql.collect_response import (
    collect_response,
)
from app.internal.repository.postgresql.connection import get_connection
from app.internal.repository.repository import Repository
from app.pkg import models

__all__ = ["Visit"]


class Visit(Repository):
    @collect_response
    async def create(self, cmd: str) -> models.Visit:
        q = """
            insert into visit(
                visit
            )
                values (
                    %(visit)s
                )
            returning id, visit;
        """
        async with get_connection() as cur:
            await cur.execute(q, {'visit': cmd})
            return await cur.fetchone()

    @collect_response
    async def read_all(self) -> List[models.Visit]:
        q = """
               select 
                    id, visit
               from visit;
           """
        async with get_connection() as cur:
            await cur.execute(q)
            return await cur.fetchall()
