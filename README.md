# Тестовое задание
С использованием:
- FastAPI
- PostgreSQL
- Docker

## Для запуска проекта

- Активировать виртуальное окружение и установить зависимости
```
poetry shell
poetry install
```
- ИЛИ: 
```
python -m venv env
.\env\Scripts\activate
pip install poetry
poetry install
```

- Скопировать содержимое .env.example в .env
- Создать БД postgres с данными из .env
- Использовать скрипт из файла init.sql для создания таблицы и данных
- Запустить проект командой 
```
uvicorn app:create_app --reload
```
- [ ] [Открыть swagger](http://127.0.0.1:8000/docs) 
- [ ] [Запись визита](http://127.0.0.1:8000) 
- [ ] [Получение списка визитов на страницу](http://127.0.0.1:8000/visit) 

## TODO
- поправить docker-compose для запуска одной командой всего проекта
- поправить миграции 
- подключить фронт
