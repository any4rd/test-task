"""
users
"""

from yoyo import step

__depends__ = {}

steps = [
    step("""
            CREATE TABLE IF NOT EXISTS users(
                id SERIAL PRIMARY KEY, 
                username varchar(255) NOT NULL, 
                birthday date NULL DEFAULT CURRENT_DATE, 
                gender bool DEFAULT FALSE, 
                created_at date NOT NULL DEFAULT NOW(), 
                updated_at date NULL, 
                images bytea NULL); 
        """,
         """
             DROP TABLE IF EXISTS users; 
         """)
]
